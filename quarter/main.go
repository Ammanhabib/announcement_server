package main

import (
	"fmt"
	"math"
	"strconv"
	"time"
)

func getQuarter(periodEnd string, yearEnd string) string {
	layout := "2006-01-02"
	y, err := time.Parse(layout, yearEnd)
	p, err := time.Parse(layout, periodEnd)
	if err != nil {
		fmt.Println(err)
	}
	months := y.Sub(p).Hours()
	year := y.Year()
	months = (months / 24) / 30
	months = (math.Round(months))
	fmt.Println(months)
	if months < 0 {
		q := math.Round(-months / 3)
		year = year + 1
		year := strconv.Itoa(year)
		return "Q" + fmt.Sprintf("%d", int(q)) + " " + year
	} else if months > 0 {

		q := math.Round(9 / months)
		year := strconv.Itoa(year)
		return "Q" + fmt.Sprintf("%d", int(q)) + " " + year

	} else {

		return "Q4" + fmt.Sprintf("%d", year)
	}
}

func main() {

	yearEnd := "2018-09-30"
	periodEnd := "2018-12-31"
	file_name := getQuarter(periodEnd, yearEnd)
	fmt.Println(file_name)
}
