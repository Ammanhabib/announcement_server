package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

//Response struct
type Response struct {
	Message string `json:"message"`
	Status  string `json:"status"`
}

//Announcement struct
type Announcement struct {
	AnnouncementAction     string `json:"AnnouncementAction"`
	AnnouncementType       string `json:"AnnouncementType"`
	AnnouncementID         string `json:"AnnouncementId"`
	SymbolCode             string `json:"SymbolCode"`
	AnnouncementTitle      string `json:"AnnouncementTitle"`
	PostingDate            string `json:"PostingDate"`
	PostingTime            string `json:"PostingTime"`
	ActionTimestamp        string `json:"ActionTimestamp"`
	Enclosure              string `json:"Enclosure"`
	Revised                string `json:"Revised"`
	OriginalAnnouncementID string `json:"OriginalAnnouncementId"`
	PeriodEnded            string `json:"Period-Ended"`
	Attachment             string `json:"Attachment"`
	CompanyName            string `json:"companyName"`
}

//serves post request of announcement
func announcements(w http.ResponseWriter, r *http.Request) {
	var ann Announcement
	message := Response{}

	switch r.Method {
	case "GET":
		http.ServeFile(w, r, "form.html")
	case "POST":
		// Call ParseForm() to parse the raw query and update r.PostForm and r.Form.
		if err := r.ParseForm(); err != nil {
			fmt.Fprintf(w, "ParseForm() err: %v", err)
		}
		//Reading from the body of post request
		b, _ := ioutil.ReadAll(r.Body)
		defer r.Body.Close()

		//Unmarshaling the json to the announcement struct
		err := json.Unmarshal(b, &ann)
		if err != nil {
			fmt.Println(err)
		}

		message.Message, err = reportUpload(ann)
		message.Status = "ok"
		if err != nil {
			message.Status = "error"
			message.Message = err.Error()
		}

		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(message)

	default:
		fmt.Fprintf(w, "Sorry, only GET and POST methods are supported.")
	}
}
func main() {
	http.HandleFunc("/", announcements)
	fmt.Printf("Starting server for testing HTTP POST for announcements...\n")
	if err := http.ListenAndServe(":8080", nil); err != nil {
		fmt.Println(err)
	}
}
