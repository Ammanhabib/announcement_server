package main

import (
	"errors"
	"fmt"
	"log"
	"math"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
)

const (
	folderPath          = "./PDF"
	annualBucket        = "test-cs-annual-reports"
	quarterlyBucket     = "test-cs-quarterly-reports"
	reportTypeAnnual    = "DARYEV1005"
	reportTypeQuarterly = "DQAV1008"
)

func reportGetQuarter(periodEnd string, yearEnd string) (string, error) {

	layout := "2006-01-02"
	y, err := time.Parse(layout, yearEnd)
	p, err := time.Parse(layout, periodEnd)
	if err != nil {
		fmt.Println(err)
	}
	months := y.Sub(p).Hours()
	year := y.Year()
	months = (months / 24) / 30
	months = (math.Round(months))
	if months >= 12 || months <= -12 {
		return "", errors.New("yearend of the company is not updated")
	}
	if months < 0 {
		q := math.Round(-months / 3)
		year := strconv.Itoa(year + 1)
		return "Q" + fmt.Sprintf("%d", int(q)) + "_" + year[2:], nil
	} else if months > 0 {
		q := math.Round(9 / months)
		year := strconv.Itoa(year)
		return "Q" + fmt.Sprintf("%d", int(q)) + "_" + year[2:], nil
	} else {
		year := strconv.Itoa(year)
		return "Q4" + "_" + year[2:], nil
	}
}

func reportReadCurrentDir(enclosure string) (*os.File, error) {
	file, err := os.Open(folderPath)
	if err != nil {
		log.Fatalf("failed opening directory: %s", err)
	}
	defer file.Close()

	list, _ := file.Readdirnames(0) // 0 to read all files and folders
	for _, name := range list {

		if name == enclosure {
			fmt.Println("File exists in the directory")
			file, err := os.Open(folderPath + "/" + name)
			return file, err

		}
	}

	return nil, errors.New("File not found in the directory")
}
func reportUpload(ann Announcement) (string, error) {
	var message string

	sess, err := session.NewSession(&aws.Config{
		Region: aws.String("us-west-2")},
	)
	cr, err := getCompanyRequest(ann.SymbolCode)
	if err != nil {
		return "yearEnd of the company not found", err
	}
	yearEnd := cr.Data.YearEnd
	date := strings.Split(yearEnd, "-")
	year := date[0]
	if ann.AnnouncementType == reportTypeQuarterly {

		fmt.Println("Announcement is of type quarterly")
		filename, err := reportGetQuarter(ann.PeriodEnded, yearEnd)
		if err != nil {
			return "", err
		}
		file, err := reportReadCurrentDir(ann.Enclosure)
		if err != nil {
			return "file not found", err
		}
		key := reportCreatePath(ann.SymbolCode, filename)
		message, err = reportUploadFile(quarterlyBucket, key, file, sess)

	} else if ann.AnnouncementType == reportTypeAnnual {

		fmt.Println("Announcement is of type yearly")
		filename := "Ann" + "_" + year[2:]
		file, err := reportReadCurrentDir(ann.Enclosure)
		if err != nil {
			return "file not found", err
		}
		key := reportCreatePath(ann.SymbolCode, filename)
		message, err = reportUploadFile(annualBucket, key, file, sess)
	}
	if err != nil {
		fmt.Println(err)
	}
	return message, err

}
func reportCreatePath(symbolCode string, key string) string {

	return symbolCode + "/" + symbolCode + "_" + key + ".pdf"
}

//AWS functions-----------------------------------------------------
func reportUploadFile(bucket string, key string, file *os.File, sess *session.Session) (string, error) {

	if reportExistsOnAWS(bucket, key, sess) {
		return "Report " + key + " already exists", nil
	}
	uploader := s3manager.NewUploader(sess)
	_, err := uploader.Upload(&s3manager.UploadInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(key),
		Body:   file,
	})
	message := "Report Uploaded successfully: " + key
	return message, err
}
func reportExistsOnAWS(bucket string, key string, sess *session.Session) bool {
	svc := s3.New(sess)
	resp, err := svc.ListObjects(&s3.ListObjectsInput{Bucket: aws.String(bucket)})
	if err != nil {
		fmt.Println(err)
	}
	for _, item := range resp.Contents {
		if *item.Key == key {
			return true
		}
	}

	return false
}
