package main

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
)

//CompanyRequest struct
type CompanyRequest struct {
	Status  string `json:"status"`
	Message string `json:"message"`
	Data    struct {
		Symbol      string `json:"symbol"`
		Description string `json:"description"`
		YearEnd     string `json:"yearEnd"`
		Secretary   string `json:"secretary"`
		Auditor     string `json:"auditor"`
		Registrar   string `json:"registrar"`
		Address     string `json:"address"`
		Website     string `json:"website"`
	} `json:"data"`
}

func getCompanyRequest(symbol string) (CompanyRequest, error) {
	var cr CompanyRequest
	url := "https://filings.capitalstake.com/fetch"
	auth := "^H4jORZu~tbBMDa$vi2%I<4WvEP}$<3hl_.gdYh){FdsV&q<;Dwgedc#O[-F2bS<"

	requestBody, _ := json.Marshal(map[string]string{
		"item":   "company",
		"symbol": symbol,
	})

	//Setting Postrequest
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(requestBody))
	if err != nil {
		log.Println(err)
	}

	req.Header.Set("Authorization", auth)
	req.Header.Set("Content-Type", "application/json")
	client := &http.Client{}

	resp, err := client.Do(req)
	if err != nil {
		log.Println(err)
	}

	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println(err)
	}

	defer resp.Body.Close()

	err = json.Unmarshal(b, &cr)
	if err != nil {
		log.Fatalln(err)
	}

	return cr, err
}
